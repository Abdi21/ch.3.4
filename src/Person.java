import java.util.Scanner;

public class Person {

    public static void main(final String[] args) {

        int age;
        String name;

        Scanner scan = new Scanner(System.in);

        System.out.println("Enter in your age.");
        age = scan.nextInt();

        if ((age >= 10) && (age < 18)) {
            System.out.println("So you're a kid, huh?");
        } else if (age < 10) {
            System.out.println("How old are you really?");
        } else if ((age >= 18) && (age <= 100)) {
            System.out.println("So you're an adult, huh?");
        } else if (age > 100) {
            System.out.println("How old are you really?");
        }

        @SuppressWarnings("resource")
        Scanner in = new Scanner(System.in);

        System.out.println("Enter in your name");
        name = in.nextLine();

        System.out.println("So you're " + age + " years old and your name is " + name + "?");

    }
}
